import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.ResolverStyle;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Alex on 5/17/2017.
 */
public class MonitoredData {

    private String activity;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }

            if (!Character.isUpperCase(c)) {
                return true;
            }
        }

        return true;
    }

    public MonitoredData(String activity, LocalDateTime sd, LocalDateTime ed){
        this.activity = activity;
        this.startTime = sd;
        this.endTime = ed;

    }

    public MonitoredData(){};

    public ArrayList<MonitoredData> fetchData() throws IOException {
        BufferedReader buf = new BufferedReader(new FileReader("Activities.txt"));
        String lineJustFetched = null;
        String[] wordsArray;
        ArrayList<MonitoredData> list = new ArrayList<>();
        while (true) {
            lineJustFetched = buf.readLine();
            if (lineJustFetched == null) {
                break;
            } else {
                wordsArray = lineJustFetched.split("\t");
                MonitoredData monitoredData = new MonitoredData();
                DateTimeFormatterBuilder fmb = new DateTimeFormatterBuilder();


                String startTime = wordsArray[0];
                String endTime = wordsArray[2];
                monitoredData.activity = wordsArray[4];

                fmb.parseCaseInsensitive();
                fmb.append(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

                LocalDateTime st = LocalDateTime.parse(startTime, fmb.toFormatter());
                LocalDateTime et = LocalDateTime.parse(endTime, fmb.toFormatter());

                monitoredData.startTime = st;
                monitoredData.endTime = et;

                //System.out.println(monitoredData.endTime.getMinute() - monitoredData.startTime.getMinute());

                list.add(monitoredData);
            }

        }

        // for(MonitoredData m: list){
        //   System.out.println(m.toString());
        //  }

        buf.close();
        return list;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime(){
        return endTime;
    }

    public String getActivity() {
        return activity;
    }

    public String toString() {
        return startTime + " " + endTime + " " + activity;
    }

    public int activityHour(){

        int resultHour ;
        if(startTime.getHour() > endTime.getHour()){
            resultHour = startTime.getHour() - endTime.getHour();
        }
        else{
            resultHour = endTime.getHour() - startTime.getHour();
        }

        return resultHour;

    }

    public int getMinutes(){

        int resultHour ;
        int resultMinute = 1000 ;
        if(startTime.getHour() > endTime.getHour()){
            resultHour = startTime.getHour() - endTime.getHour();
        }
        else{
            resultHour = endTime.getHour() - startTime.getHour();
        }

        if(resultHour == 0){

            resultMinute = endTime.getMinute() - startTime.getMinute();
        }


        return resultMinute;
    }


    public int getDay(){
        return this.getStartTime().getDayOfMonth();
    }


    public void countDistinctDays(ArrayList<MonitoredData> list) {

        List<LocalDateTime> getStartTimes = list.stream().map(MonitoredData::getStartTime).collect(Collectors.toList());
        List<Integer> getDays = getStartTimes.stream().map(LocalDateTime::getDayOfMonth).collect(Collectors.toList());
        long countDays = getDays.stream().distinct().count();
        System.out.println("The number of distinct days in the monitored data is: " + countDays);

    }

    public void task2(ArrayList<MonitoredData> list) throws FileNotFoundException, UnsupportedEncodingException {

        List<String> activities = list.stream().map(MonitoredData::getActivity).collect(Collectors.toList());

        Stream<String> s = activities.stream();

        PrintWriter writer = new PrintWriter("task2.txt", "UTF-8");

        Map<String, Integer> mapInt = s.collect(Collectors.groupingBy(
                Function.identity(),
                Collectors.reducing(0, str -> 1, Integer::sum)));

        mapInt.forEach((k, v) -> writer.println(k + " ->" + v));

        writer.close();
    }


    public void task3(List<MonitoredData> list) throws FileNotFoundException, UnsupportedEncodingException {

        PrintWriter writer = new PrintWriter("task3.txt", "UTF-8");

        Map<Integer,Map<String,Long>> theMap = list.stream().collect(Collectors.groupingBy(MonitoredData::getDay,Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));

        theMap.forEach((k,v)-> writer.println(k+ " "+v));

        writer.close();

    }

    public void task4(List<MonitoredData> list) throws FileNotFoundException, UnsupportedEncodingException {


      //  Map<String,Integer> dateMap = list.stream().distinct().collect(Collectors.toMap(MonitoredData::getActivity,MonitoredData::activityHour));

      //  Map<String,Integer> result = list.stream().collect(Collectors.toMap(MonitoredData::getActivity,Function.identity()));
      //  dateMap.forEach((k,v) -> System.out.println(k+ " " +v));

        Map<String,Integer> result = list.stream().collect(Collectors.toMap(data->data.getActivity(),data->data.activityHour(),(d1,d2)->d1));
      //  Map<List<String>,Integer> result = list.stream().collect(Collectors.groupingBy()
       // result.forEach((k,v) -> System.out.println(k+ " " +v));

        int sum = list.stream().mapToInt(w->w.activityHour()).sum();

       // System.out.println(sum);

    }


    public void task5(List<MonitoredData> list) throws FileNotFoundException, UnsupportedEncodingException{

        PrintWriter writer = new PrintWriter("task5.txt", "UTF-8");

        List<String> activities = list.stream().filter((x)->(x.getMinutes()<5 )).map(MonitoredData::getActivity).distinct().collect(Collectors.toList());
        activities.forEach((activity) -> writer.println(activity));

        writer.close();

    }

    public static void main(String[] args) {
        MonitoredData data = new MonitoredData();
        ArrayList<MonitoredData> list = new ArrayList<>();

        try {
            list = data.fetchData();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        data.countDistinctDays(list);

        try {
            data.task2(list);
        } catch (IOException e2) {
            e2.printStackTrace();
        }

        try {
            data.task5(list);
        } catch (IOException e3) {
            e3.printStackTrace();
        }

        try {
            data.task4(list);
        } catch (IOException e3) {
            e3.printStackTrace();
        }

        try {
            data.task3(list);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

}
